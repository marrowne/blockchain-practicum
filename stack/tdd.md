# Test-riven development (TDD)
Test-driven development (TDD) is a software development process that relies on the repetition of a very short development cycle: requirements are turned into very specific *test cases*, then the software is improved to *pass* the new tests.

TDD cycle:
* Add a test
* Run all tests and see if the new test fails
* Write the code
* Run tests
* Refactor code
