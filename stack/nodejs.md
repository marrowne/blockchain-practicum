# Node.js

Node.js is an open-source, cross-platform JavaScript run-time environment that executes JavaScript code outside of a browser. 

* created in 2009 by Ryan Dahl
* supported on Linux, macOS, Windows, FreeBSD etc…
* build on top of [Chrome V8 engine](v8.md)
* included modules for file system I/O, networking, cryptography, buffers, etc…
* non-blocking callback-style functions
* event-driven programming
